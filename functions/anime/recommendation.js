export const onRequestPost = async ({ request, env }) => {
  try {
    if (request.headers.get('Content-Type') != "application/json") {
      return new Response("Content-Type wrong", { status: 400 });
    }

    let kv = env.ANIME_KV;
    let rec = await request.json();
    rec = {
      recommendation: rec.recommendation,
      chosen_path: rec.chosen_path,
    };
    await kv.put(`rec_${Date.now().toString().padStart(20, "0")}_${crypto.randomUUID()}`, JSON.stringify({
      ip: request.headers.get("CF-Connecting-IP"),
      time: Date.now(),
      rec
    }));
    return new Response("OK", { status: 200 });
  } catch (e) {
    return new Response(e.toString(), { status: 500 });
  }
};
