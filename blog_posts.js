let ph = document.querySelector("#blog-posts-ph");
let container = document.createElement("div");
container.classList.add("blog-posts");
ph.parentElement.replaceChild(container, ph);

container.innerHTML = '<div class="loading">Loading blog posts</div>'

let feed_url = "https://blog.maowtm.org/feed.json";

async function load_posts() {
  let response = await fetch(feed_url, { method: "GET", credentials: "omit" });
  if (!response.ok) {
    throw new Error(`Failed to fetch ${feed_url}: ${response.status} ${response.statusText}`);
  }
  let feed = await response.json();
  container.innerHTML = "";
  let count = 0;
  for (let post of feed) {
    count++;
    if (count > 6) {
      let more_container = document.createElement("div");
      more_container.classList.add("blog-posts-more");
      let more = document.createElement("a");
      more.href = "https://blog.maowtm.org/";
      more.target = "_blank";
      more.textContent = "Read more";
      more_container.appendChild(more);
      container.after(more_container);
      break;
    }
    let card = document.createElement("a");
    card.classList.add("post");
    card.href = post.url;
    card.target = "_blank";
    container.appendChild(card);

    if (post.cover_image) {
      let cover_img = document.createElement("img");
      cover_img.src = post.cover_image;
      cover_img.classList.add("cover");
      card.appendChild(cover_img);
    }

    let title = document.createElement("div");
    title.classList.add("title");
    title.textContent = post.title;
    card.appendChild(title);

    let snippet = document.createElement("p");
    snippet.classList.add("snippet");
    snippet.textContent = post.snippet;
    card.appendChild(snippet);
  }
  return feed;
}

load_posts().catch((err) => {
  container.innerHTML = `<div class="error"></div>`;
  container.querySelector(".error").textContent = err.message;
});
