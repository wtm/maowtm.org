document.addEventListener('DOMContentLoaded', function () {
  let terminal = document.querySelector('.yuki-terminal');
  start(terminal);
});

async function start(terminal) {
  terminal.style.display = 'block';
  let spans = Array.from(terminal.querySelectorAll(':scope > span'));
  let cursor = document.createElement("span");
  cursor.innerHTML = "_";
  cursor.classList.add('cursor');
  terminal.insertBefore(cursor, spans[0]);
  for (let s of spans) {
    s.style.display = 'none';
  }

  let choices = [];
  let wait_callback = null;

  for (let s of spans) {
    if (s.classList.contains('choice')) {
      choices.push(s);
      s.tabIndex = 0;
      s.addEventListener("click", handle_choice);
      s.classList.add("disabled");
    }
  }

  function wait() {
    if (wait_callback) {
      throw new Error("wait() called in parallel");
    }
    for (let c of choices) {
      if (!c.classList.contains("stay-disabled-effective")) {
        c.classList.remove("disabled");
      }
    }
    return new Promise((resolve) => {
      wait_callback = () => {
        for (let c of choices) {
          c.classList.add("disabled");
        }
        wait_callback = null;
        resolve();
      };
    });
  }

  let ptr = 0;
  function reset_to(ptr) {
    for (let i = ptr + 1; i < spans.length; i++) {
      spans[i].style.display = 'none';
      spans[i].classList.remove("chosen");
      spans[i].classList.remove("stay-disabled-effective");
    }
  }
  function goto(new_id) {
    let new_ptr = spans.findIndex(s => s.id == new_id);
    if (new_ptr == -1) {
      alert(`goto(${new_id}) invalid`);
      throw new Error(`goto(${new_id}) invalid`);
    }
    ptr = new_ptr;
  }

  function handle_choice(evt) {
    if (!wait_callback) {
      return;
    }
    let chosen = null;
    for (let c of choices) {
      if (c == evt.target || c.contains(evt.target)) {
        c.classList.add("chosen");
        chosen = c;
      } else if (c.dataset.group == evt.target.dataset.group) {
        c.classList.remove("chosen");
      }
    }
    if (chosen !== null && !chosen.classList.contains("disabled")) {
      let skipTo = chosen.dataset.skipTo;
      if (chosen.classList.contains("stay-disabled")) {
        chosen.classList.add("stay-disabled-effective");
      }
      // find next wait-choice
      for (let i = spans.indexOf(chosen) + 1; i < spans.length; i++) {
        if (spans[i].classList.contains('wait-choice')) {
          reset_to(i);
          break;
        }
      }
      goto(skipTo);
      wait_callback();
    }
  }

  if (window.location.hash.startsWith("#_goto_")) {
    let goto_target = window.location.hash.substring(7);
    goto(goto_target);
  }

  while (true) {
    if (ptr >= spans.length) {
      await wait();
    }

    let s = spans[ptr];
    if (s.dataset["if"]) {
      let cond = s.dataset["if"];
      let cond_choice = choices.find(c => c.dataset.skipTo == cond);
      if (!cond_choice || !cond_choice.classList.contains("chosen")) {
        ptr += 1;
        continue;
      }
    }

    let delay_dur = parseInt(s.dataset.delay || "0");
    let delay_per_char = parseInt(s.dataset.delayPerChar || "0");
    if (delay_dur) {
      await sleep(delay_dur);
    }
    s.style.display = '';
    if (s.nextSibling) {
      terminal.insertBefore(cursor, s.nextSibling);
    } else {
      terminal.appendChild(cursor);
    }
    if (delay_per_char) {
      let textContent = s.textContent;
      s.innerHTML = "";
      for (let i = 0; i < textContent.length; i++) {
        await sleep(delay_per_char);
        let char = document.createTextNode(textContent[i]);
        s.appendChild(char);
      }
    }

    if (s.dataset.skipTo && !s.classList.contains("choice")) {
      goto(s.dataset.skipTo);
      continue;
    }

    if (s.id == "anime-recommendation-submit") {
      let success = await handle_recommendation_submit();
      if (success) {
        reset_to(ptr - 1);
        goto("anime-recommendation-success");
        let no_btn = document.getElementById("anime-recommendation-no-btn");
        no_btn.classList.add("stay-disabled-effective");
        let wait_span = document.getElementById("anime-recommendation-wait");
        wait_span.dataset.skipTo = "anime-recommendation-success";
        wait_span.classList.remove("wait-choice");
        continue;
      } else {
        reset_to(ptr - 1);
        goto("anime-recommendation-wait");
        continue;
      }
    }

    if (s.classList.contains("wait-choice")) {
      await wait();
    } else {
      ptr += 1;
    }
  }
}

function sleep(ms) {
  if (window.location.hash == "#debug") {
    return new Promise(resolve => setTimeout(resolve, 0));
  }
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function handle_recommendation_submit() {
  let wait_txt = document.getElementById("anime-recommendation-wait");
  wait_txt.innerHTML = "";
  wait_txt.style.color = "";
  let input_box = document.getElementById("anime-recommendation-input");
  try {
    let response = await fetch("/anime/recommendation", {
      method: "POST",
      body: JSON.stringify({
        recommendation: input_box.value,
        chosen_path: Array.from(document.querySelectorAll(".choice.chosen")).map(c => c.dataset.skipTo)
      }),
      headers: {
        "Content-Type": "application/json"
      }
    });
    if (!response.ok) {
      throw new Error(`${response.status} ${response.statusText}`);
    }
    return true;
  } catch (e) {
    wait_txt.textContent = "Error: " + e.message;
    wait_txt.style.color = "red";
    wait_txt.appendChild(document.createElement("br"));
    return false;
  }
}
